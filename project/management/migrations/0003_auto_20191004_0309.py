# Generated by Django 2.0.3 on 2019-10-03 19:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0002_medicalrecord_medicine_used'),
    ]

    operations = [
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('patient_type', models.CharField(choices=[('s', 'Student'), ('sm', 'Staff Member')], default='s', max_length=11)),
                ('first_name', models.CharField(max_length=50, null=True)),
                ('middle_name', models.CharField(blank=True, max_length=50)),
                ('last_name', models.CharField(max_length=50, null=True)),
                ('gender', models.CharField(choices=[('m', 'Male'), ('f', 'Female')], default='m', max_length=11)),
                ('civil_status', models.CharField(choices=[('s', 'Single'), ('m', 'Married'), ('w', 'Widowed'), ('d', 'Divorced'), ('l', 'Legally Separated')], default='s', max_length=11)),
                ('date_of_birth', models.DateField(null=True)),
                ('age', models.PositiveIntegerField(default=0, null=True)),
                ('full_address', models.CharField(blank=True, max_length=100)),
                ('is_active', models.BooleanField(default=True)),
                ('profile_photo', models.FileField(blank=True, null=True, upload_to='')),
                ('section', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='management.Section')),
            ],
            options={
                'ordering': ('pk',),
            },
        ),
        migrations.RemoveField(
            model_name='student',
            name='section',
        ),
        migrations.RemoveField(
            model_name='medicalrecord',
            name='student',
        ),
        migrations.DeleteModel(
            name='Student',
        ),
        migrations.AddField(
            model_name='medicalrecord',
            name='patient',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='management.Patient'),
        ),
    ]
