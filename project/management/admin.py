from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import Patient, MedicalRecord, Condition, Section, GradeLevel

@admin.register(Patient)
class PatientAdmin(ImportExportModelAdmin):
    pass

@admin.register(Condition)
class ConditionAdmin(ImportExportModelAdmin):
    pass

@admin.register(MedicalRecord)
class MedicalRecordAdmin(ImportExportModelAdmin):
    pass

@admin.register(Section)
class SectionAdmin(ImportExportModelAdmin):
    pass

@admin.register(GradeLevel)
class GradeLevelAdmin(ImportExportModelAdmin):
    pass
