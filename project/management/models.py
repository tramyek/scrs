from django.contrib.auth.models import User
# from django.dispatch import receiver
# from django.db.models.signals import post_save
from django.db import models
from datetime import date
import datetime

GENDER_CHOICES = (
    ('m', 'Male'),
    ('f', 'Female'),
)

PATIENT_CHOICES = (
    ('s', 'Student'),
    ('sm', 'Staff Member'),
)

CSTATUS_CHOICES = (
    ('s', 'Single'),
    ('m', 'Married'),
    ('w', 'Widowed'),
    ('d', 'Divorced'),
    ('l', 'Legally Separated'),
)

BMICATEGORY_CHOICES = (
    ('vsu', 'Very severely underweight'),
    ('su', 'Severely underweight'),
    ('u', 'Underweight'),
    ('n', 'Normal'),
    ('o', 'Overweight'),
    ('mo', 'Moderately obese'),
    ('so', 'Severely obese'),
    ('vso', 'Very severely obese'),	
)

class GradeLevel(models.Model):
    grade_level = models.CharField(max_length=50)

    class Meta:
        ordering = ('pk',)

    def __str__(self):
        return self.grade_level

class Section(models.Model):
    grade_level = models.ForeignKey(GradeLevel, on_delete=models.SET_NULL, null=True)
    name = models.CharField(max_length=254)

    class Meta:
        ordering = ('pk',)

    def __str__(self):
        return '{} {}'.format(self.grade_level, self.name)

class Patient(models.Model):
    section = models.ForeignKey(Section, on_delete=models.SET_NULL, null=True)
    patient_type = models.CharField(max_length=11, choices=PATIENT_CHOICES, default='s')
    first_name = models.CharField(max_length=50, null=True)
    middle_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, null=True)
    gender = models.CharField(max_length=11, choices=GENDER_CHOICES, default='m')
    civil_status = models.CharField(max_length=11, choices=CSTATUS_CHOICES, default='s')
    date_of_birth = models.DateField(null=True)
    age = models.PositiveIntegerField(default=0, null=True)
    full_address = models.CharField(max_length=100, blank=True)
    is_active = models.BooleanField(default=True)
    profile_photo = models.FileField(blank=True, null=True)

    class Meta:
        ordering = ('pk',)
            
    @property
    def image_url(self):
        if self.profile_photo and hasattr(self.profile_photo, 'url'):
            return self.profile_photo.url

    def __str__(self):
        return '{} {} {}'.format(self.first_name, self.middle_name, self.last_name)

    def get_age(self):
        today = date.today()
        try:
            birthday = self.date_of_birth.replace(year=today.year)
        except ValueError: # raised when birth date is Feb 29 and the current year is not a leap year
            birthday = self.date_of_birth.replace(year=today.year, month=birthday.month+1, day=1)
        if birthday > today:
            return today.year - self.date_of_birth.year - 1
        else:
            return today.year - self.date_of_birth.year

    def get_full_address(self):
        return '{}'.format(self.street_address)

    def get_full_name(self):
        return '{} {} {}'.format(self.first_name, self.middle_name, self.last_name)

    def get_section(self):
        return '{}'.format(self.section)

    def save(self):
        self.age = self.get_age()
        super(Patient, self).save()

class Condition(models.Model):
    name = models.CharField(max_length=254, unique=True)
    description = models.TextField(max_length=4000, blank=True)

    class Meta:
        ordering = ('pk',)

    def __str__(self):
        return self.name

class MedicalRecord(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.SET_NULL, null=True)
    height = models.PositiveIntegerField('Height(cm)', default=0, help_text='Height in cm')
    weight = models.PositiveIntegerField('Weight(kg)', default=0, help_text="Weight in kg")
    bmi = models.FloatField(blank=True)
    bmi_category = models.CharField(max_length=50, choices=BMICATEGORY_CHOICES, blank=True)
    summarized_medical_record = models.TextField(max_length=4000)
    condition = models.ForeignKey(Condition, on_delete=models.SET_NULL, null=True, blank=True)
    medicine_used = models.CharField(max_length=100, unique=False, blank=True)
    reason_of_admission = models.CharField(max_length=100, unique=False)
    checkup_date = models.DateTimeField()

    class Meta:
        ordering = ('-checkup_date',)

    def __str__(self):
        return 'Medical Record No.{} of {}'.format(self.pk, self.Patient)
		
    def get_bmi(self):
        height_in_meters = self.height*0.01
        bmi = self.weight/(height_in_meters*height_in_meters)
        return str(bmi)[:5]

    def get_bmi_category(self):
        height_in_meters = float(self.height*0.01)
        bmi = self.weight/(height_in_meters*height_in_meters)
        if(bmi < 15):
            bmi_category = "vsu"
        elif(bmi > 15 and bmi < 16) :
            bmi_category = "su"
        elif(bmi > 16 and bmi < 18.5) :
            bmi_category = "u"
        elif(bmi > 18.5 and bmi < 25 ):
            bmi_category = "n"
        elif(bmi > 25 and bmi < 30):
            bmi_category = "o"
        elif(bmi > 30 and bmi < 35):
            bmi_category = "mo"
        elif(bmi > 35 and bmi < 40):
            bmi_category = "so"
        elif(bmi > 40):
            bmi_category = "vso"
        return bmi_category
		
    def save(self):
        self.bmi = self.get_bmi()
        self.bmi_category = self.get_bmi_category()
        super(MedicalRecord, self).save()
