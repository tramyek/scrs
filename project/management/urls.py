from django.urls import path, include
from django.contrib.auth import views as auth_views

from . import views

# Autentication view routing
auth_urls = [
    path('login', auth_views.LoginView.as_view(template_name='auth/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('change-password/', auth_views.PasswordChangeView.as_view(template_name='auth/password_change.html'), name='password_change'),
    path('change-password-done/', auth_views.PasswordChangeDoneView.as_view(template_name='auth/password_change_done.html'), name='password_change_done'), 
]

# Account view routing
account_urls = [
    path('', views.account_list, name='account_list'),
    path('new', views.account_add, name='account_add'),
    path('<int:pk>/edit', views.account_edit, name='account_edit'),
    path('profile', views.account_profile, name='account_profile'),
]

# Student view routing
student_urls = [
    path('', views.student_list, name='student_list'),
    path('new', views.student_add, name='student_add'),
    path('<int:pk>/edit', views.student_edit, name='student_edit'),
    path('<int:pk>/detail', views.student_detail, name='student_detail'),
    path('<int:pk>/medical-record/new/', views.medical_for_student, name='medical_for_student'),
]

# Staff Member view routing
staff_member_urls = [
    path('', views.staff_member_list, name='staff_member_list'),
    path('new', views.staff_member_add, name='staff_member_add'),
    path('<int:pk>/edit', views.staff_member_edit, name='staff_member_edit'),
    path('<int:pk>/detail', views.staff_member_detail, name='staff_member_detail'),
    path('<int:pk>/medical-record/new/', views.medical_for_staff_member, name='medical_for_staff_member'),
]

# Record view routing
record_urls = [
    path('medical', views.medical_record_list, name='medical_record_list'),
    path('medical/new', views.medical_record_add, name='medical_record_add'),
    path('medical/<int:pk>/edit', views.medical_record_edit, name='medical_record_edit'),
    path('condition', views.condition_list, name='condition_list'),
    path('condition/new', views.condition_add, name='condition_add'),
    path('condition/<int:pk>/edit/', views.condition_edit, name='condition_edit'),
    path('section', views.section_list, name='section_list'),
    path('section/new', views.section_add, name='section_add'),
    path('section/<int:pk>/edit/', views.section_edit, name='section_edit'),
    path('grade-level', views.grade_level_list, name='grade_level_list'),
    path('grade-level/new', views.grade_level_add, name='grade_level_add'),
    path('grade-level/<int:pk>/edit/', views.grade_level_edit, name='grade_level_edit'),
]

# Import export views
data_urls = [
    path('export-students/<str:type>/', views.export_students, name='export_students'),
    path('export-staff-members/<str:type>/', views.export_staff_members, name='export_staff_members'),
    path('export-medical/<int:pk>/<str:type>/', views.export_medical_student, name='export_medical_student'),
    path('export-medical/<int:pk>/<str:type>/', views.export_medical_staff_member, name='export_medical_staff_member'),
    path('export-medical<str:type>/', views.export_medical, name='export_medical'),
    path('export-condition/<str:type>/', views.export_conditions, name='export_conditions'),
    path('export-grade_level<str:type>/', views.export_grade_level, name='export_grade_level'),
    path('export-section<str:type>/', views.export_section, name='export_section'),
]

# Main URL routing patterns
urlpatterns = [
    path('', views.home, name='home'),
    path('auth/', include(auth_urls)),
    path('accounts/', include(account_urls)),
    path('students/', include(student_urls)),
    path('staff-members/', include(staff_member_urls)),
    path('records/', include(record_urls)),
    path('data/', include(data_urls))
]