from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

from .models import Patient, Condition, MedicalRecord, GradeLevel, Section

# User Account Forms
class NewAccountForm(UserCreationForm):
    username = forms.CharField(help_text='Username. Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. ')
    first_name = forms.CharField(required=True, help_text='First Name. Required. Example: John')
    last_name = forms.CharField(required=True, help_text='Last Name. Required. Example: Doe')
    email = forms.CharField(
        max_length=254,
        required=True,
        help_text='Email. Required. Example: username@gmail.com',
        widget=forms.EmailInput()
    )
    is_active = forms.BooleanField(help_text="Designates whether this account should be treated as active. Uncheck this instead of deleting accounts.")
    is_staff = forms.BooleanField(help_text="Designates whether this account should be an administrative account.")
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'is_active', 'is_staff', 'password1', 'password2')

class AccountForm(forms.ModelForm):
    '''Form for updating base User Model'''
    username = forms.CharField(help_text='Username. Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. ')
    first_name = forms.CharField(required=True, help_text='First Name. Required. Example: John')
    last_name = forms.CharField(required=True, help_text='Last Name. Required. Example: Doe')
    email = forms.CharField(
        max_length=254,
        required=True,
        help_text='Email. Required. Example: username@gmail.com',
        widget=forms.EmailInput()
    )
    is_active = forms.BooleanField(required=False, help_text="Designates whether this account should be treated as active. Uncheck this instead of deleting accounts.")
    is_staff = forms.BooleanField(required=False, help_text="Designates whether this account should be an administrative account.")
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'is_active', 'is_staff')
# --


# Condition Forms
class ConditionForm(forms.ModelForm):
    '''Form for updating and adding Conditions'''
    name = forms.CharField(help_text='Condition Name. Required.')
    description = forms.CharField(required=False, help_text='Condition Description. Not Required.')

    class Meta:
        model = Condition
        fields = ('name', 'description')
# --

# Grade Forms
class GradeLevelForm(forms.ModelForm):
    '''Form for updating and adding Grade Levels'''
    grade_level = forms.CharField(help_text='Grade level. Required.')

    class Meta:
        model = GradeLevel
        fields = ('grade_level', )
# -

class GradeLevelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        grade_level = obj
        return '{}'.format(grade_level)
# --

# Section Forms
class SectionForm(forms.ModelForm):
    '''Form for updating and adding Sections'''
    grade_level = GradeLevelChoiceField(queryset=GradeLevel.objects.all(), required=True, help_text='Grade level. Required. Select Grade level')
    name = forms.CharField(help_text='Section Name. Required.')

    class Meta:
        model = Section
        fields = ('grade_level', 'name')
# --

# Patient Forms
class SectionChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        section_grade_level = obj.grade_level
        section_name = obj.name
        return '({grade_level}) {name}'.format(grade_level=section_grade_level, name=section_name)

class PatientForm(forms.ModelForm):
    '''Form for creating and updating Patients'''
    CSTATUS_CHOICES = (
        ('s', 'Single'),
        ('m', 'Married'),
        ('w', 'Widowed'),
        ('d', 'Divorced'),
        ('l', 'Legally Separated'),
    )
    GENDER_CHOICES = (
        ('m', 'Male'),
        ('f', 'Female'),
    )
    PATIENT_CHOICES = (
        ('s', 'Student'),
        ('sm', 'Staff Member'),
    )
    section = SectionChoiceField(queryset=Section.objects.all(), required=False, help_text='Section. Not Required. Select Section')
    first_name = forms.CharField(required=True, help_text='First Name. Required. ie: John')
    middle_name = forms.CharField(required=False, help_text='Middle Name. Not Required. ie: Sy')
    last_name = forms.CharField(required=True, help_text='Last Name. Required. ie: Doe')
    gender = forms.ChoiceField(required=True, choices=GENDER_CHOICES, help_text='Gender. Required. Select gender')
    civil_status = forms.ChoiceField(required=True, choices=CSTATUS_CHOICES, help_text='Civil Status. Required. Select civil status')
    date_of_birth = forms.DateField(required=True, help_text='Date of Birth. Required. Format (YYYY-MM-DD)', widget=forms.TextInput(attrs={'class':'datepicker'}))
    full_address = forms.CharField(required=False, max_length=254, help_text="Please enter address")
    is_active = forms.BooleanField(required=False, help_text="Designates whether this person's record should be treated as active. Uncheck this instead of deleting Patients.")
    class Meta:
        model = Patient
        fields = ('section', 'first_name', 'middle_name', 'last_name', 'gender', 'date_of_birth', 'full_address', 'is_active', 'profile_photo')

class PatientChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        Patient_no = obj.pk
        Patient_full_name = obj.get_full_name()
        return '({no}) {name}'.format(no=Patient_no, name=Patient_full_name)
# --

# Medical Record
class MedicalRecordForm(forms.ModelForm):
    '''Form for creating and updating Medical Records'''
    patient = PatientChoiceField(queryset=Patient.objects.all(), required=True, help_text='Patient. Required. Select Patient')
    condition = forms.ModelChoiceField(required=True, queryset=Condition.objects.all(), help_text='Condition. Not Required. Select Condition')
    medicine_used = forms.CharField(required=True, help_text='Medicine used. Required. Specify medicine administered.')
    height = forms.CharField(required=True, help_text='Height. Required. In (cm)', max_length=4, widget=forms.TextInput(attrs={'max': 9999, 'min': 1}))
    weight = forms.CharField(required=True, help_text='Weight. Required. In (kg)', max_length=4, widget=forms.TextInput(attrs={'max': 9999, 'min': 1}))
    bmi = forms.CharField(required=False, help_text='BMI. Autocalculated', widget=forms.TextInput(attrs={'readonly':True, 'max': 9999, 'min': 1}))    
    checkup_date = forms.DateTimeField(required=True, help_text='Checkup Date. Required. Format (YYYY-MM-DD HH:MM)', widget=forms.TextInput(attrs={'class':'datepicker'}))
    reason_of_admission = forms.CharField(required=True, help_text='Reason of admission. Required. Specify reason for admission')
    summarized_medical_record = forms.CharField(
        widget=forms.Textarea(
            attrs={'placeholder': "Summarized medical record of the Patient"}
        ),
        max_length=4000,
        help_text='The max length of the text is 4000.'
    )
    class Meta:
        model = MedicalRecord
        fields = ('patient', 'condition', 'checkup_date', 'reason_of_admission', 'summarized_medical_record', 'height', 'weight', 'medicine_used')

class EditMedicalRecordForm(forms.ModelForm):
    '''Form for creating and updating Medical Records'''
    condition = forms.ModelChoiceField(required=True, queryset=Condition.objects.all(), help_text='Condition. Not Required. Select Condition')
    medicine_used = forms.CharField(required=True, help_text='Medicine used. Required. Specify medicine administered.')
    height = forms.CharField(required=True, help_text='Height. Required. In (cm)', max_length=4, widget=forms.TextInput(attrs={'max': 9999, 'min': 1}))
    weight = forms.CharField(required=True, help_text='Weight. Required. In (kg)', max_length=4, widget=forms.TextInput(attrs={'max': 9999, 'min': 1}))
    bmi = forms.CharField(required=False, help_text='BMI. Autocalculated', widget=forms.TextInput(attrs={'readonly':True, 'max': 9999, 'min': 1}))    
    checkup_date = forms.DateTimeField(required=True, help_text='Checkup Date. Required. Format (YYYY-MM-DD HH:MM)', widget=forms.TextInput(attrs={'class':'datepicker'}))
    reason_of_admission = forms.CharField(required=True, help_text='Reason of admission. Required. Specify reason for admission')
    summarized_medical_record = forms.CharField(
        widget=forms.Textarea(
            attrs={'placeholder': "Summarized medical record of the Patient"}
        ),
        max_length=4000,
        help_text='The max length of the text is 4000.'
    )
    class Meta:
        model = MedicalRecord
        fields = ('condition', 'checkup_date', 'reason_of_admission', 'summarized_medical_record', 'height', 'weight', 'medicine_used')
        
class PatientMedicalRecordForm(forms.ModelForm):
    '''Form for creating and updating Medical Records'''
    condition = forms.ModelChoiceField(required=True, queryset=Condition.objects.all(), help_text='Condition. Not Required. Select Condition')
    medicine_used = forms.CharField(required=True, help_text='Medicine used. Required. Specify medicine administered.')
    height = forms.CharField(required=True, help_text='Height. Required. In (cm)', max_length=4, widget=forms.TextInput(attrs={'max': 9999, 'min': 1}))
    weight = forms.CharField(required=True, help_text='Weight. Required. In (kg)', max_length=4, widget=forms.TextInput(attrs={'max': 9999, 'min': 1}))
    bmi = forms.CharField(required=False, help_text='BMI. Autocalculated', widget=forms.TextInput(attrs={'readonly':True, 'max': 9999, 'min': 1}))    
    checkup_date = forms.DateTimeField(required=True, help_text='Checkup Date. Required. Format (YYYY-MM-DD HH:MM)', widget=forms.TextInput(attrs={'class':'datepicker'}))
    reason_of_admission = forms.CharField(required=True, help_text='Reason of admission. Required. Specify reason for admission')
    summarized_medical_record = forms.CharField(
        widget=forms.Textarea(
            attrs={'placeholder': "Summarized medical record of the Patient"}
        ),
        max_length=4000,
        help_text='The max length of the text is 4000.'
    )
    class Meta:
        model = MedicalRecord
        fields = ('condition', 'checkup_date', 'reason_of_admission', 'summarized_medical_record', 'height', 'weight', 'medicine_used')