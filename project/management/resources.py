from import_export import resources
from .models import Patient, MedicalRecord, GradeLevel, Section, Condition

class PatientResource(resources.ModelResource):
    class Meta:
        model = Patient

class GradeLevelResource(resources.ModelResource):
    class Meta:
        model = GradeLevel
		
class MedicalRecordResource(resources.ModelResource):
    class Meta:
        model = MedicalRecord
		
class SectionResource(resources.ModelResource):
    class Meta:
        model = Section

class ConditionResource(resources.ModelResource):
    class Meta:
        model = Condition
				