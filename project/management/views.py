''' Views / Pages '''

# Imports
# Django imports
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.db.models import Avg, Max, Min, Sum

# Third party imports
from tablib import Dataset

# Developer defined imports
from .forms import AccountForm, NewAccountForm, PatientForm, MedicalRecordForm, \
    SectionForm, GradeLevelForm, ConditionForm, PatientMedicalRecordForm, EditMedicalRecordForm
from .models import Patient, Condition, MedicalRecord, GradeLevel, Section
from .resources import PatientResource, ConditionResource, MedicalRecordResource, \
    SectionResource, GradeLevelResource

## Developer defined views 
# General views
@login_required
def home(request, template_name='management/home.html'):
    ''' Homepage '''
    data = {}
    data['section'] = 'Home'
    return render(request, template_name, data)

# Import Export Views
# Medical record import method
@login_required
@user_passes_test(lambda u: u.is_staff)
def import_med(request):
    ''' Import medical records '''
    if request.method == 'POST':
        medical_resource = MedicalRecordResource()
        dataset = Dataset()
        new_medical_records = request.FILES['medical']

        imported_data = dataset.load(new_medical_records.read())
        result = medical_resource.import_data(dataset, dry_run=True) # Test the data import

        if not result.has_errors():
            medical_resource.import_data(dataset, dry_run=False) # Actually import now

@user_passes_test(lambda u: u.is_staff)
def export_students(request, type):
    student_resource = PatientResource()
    dataset = student_resource.export(Patient.objects.filter(patient_type="s"))
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="students.csv"'
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="students.json"'
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="students.xls"'
    return response

@user_passes_test(lambda u: u.is_staff)
def export_staff_members(request, type):
    staff_member_resource = PatientResource()
    dataset = staff_member_resource.export(Patient.objects.filter(patient_type="sm"))
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="staff_members.csv"'
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="staff_members.json"'
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="staff_members.xls"'
    return response

@user_passes_test(lambda u: u.is_staff)
def export_conditions(request, type):
    condition_resource = ConditionResource()
    dataset = condition_resource.export()
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="conditions.csv"'
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="conditions.json"'
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="conditions.xls"'
    return response
	
@user_passes_test(lambda u: u.is_staff)
def export_medical_staff_member(request, type, pk):
    staff_member = get_object_or_404(Patient, pk=pk)
    medical_resource = MedicalRecordResource()
    dataset = medical_resource.export(MedicalRecord.objects.filter(patient=staff_member))
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="medical_records_of_{}-{}-{}.csv"'.format((staff_member.first_name).lower(), (staff_member.middle_name).lower(), (staff_member.last_name).lower())
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="medical_records_of_{}-{}-{}.json"'.format((staff_member.first_name).lower(), (staff_member.middle_name).lower(), (staff_member.last_name).lower())
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="medical_records_of_{}-{}-{}.xls"'.format((staff_member.first_name).lower(), (staff_member.middle_name).lower(), (staff_member.last_name).lower())
    return response	

@user_passes_test(lambda u: u.is_staff)
def export_medical_student(request, type, pk):
    student = get_object_or_404(Patient, pk=pk)
    medical_resource = MedicalRecordResource()
    dataset = medical_resource.export(MedicalRecord.objects.filter(patient=student))
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="medical_records_of_{}-{}-{}.csv"'.format((student.first_name).lower(), (student.middle_name).lower(), (student.last_name).lower())
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="medical_records_of_{}-{}-{}.json"'.format((student.first_name).lower(), (student.middle_name).lower(), (student.last_name).lower())
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="medical_records_of_{}-{}-{}.xls"'.format((student.first_name).lower(), (student.middle_name).lower(), (student.last_name).lower())
    return response	
	
@user_passes_test(lambda u: u.is_staff)
def export_medical(request, type):
    medical_resource = MedicalRecordResource()
    dataset = medical_resource.export()
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="medical_records.csv"'
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="medical_records.json"'
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="medical_records.xls"'
    return response	

@user_passes_test(lambda u: u.is_staff)
def export_grade_level(request, type):
    grade_level_resource = GradeLevelResource()
    dataset = grade_level_resource.export()
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="grade_level_records.csv"'
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="grade_level_records.json"'
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="grade_level_records.xls"'
    return response

@user_passes_test(lambda u: u.is_staff)
def export_section(request, type):
    section_resource = SectionResource()
    dataset = section_resource.export()
    if type == 'csv':
        response = HttpResponse(dataset.csv, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="section_records.csv"'
    elif type == 'json':
        response = HttpResponse(dataset.json, content_type='application/json')
        response['Content-Disposition'] = 'attachment; filename="section_records.json"'
    elif type == 'xls':
        response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename="section_records.xls"'
    return response	
# --

# Student views
@login_required
def student_list(request, template_name='management/records/students/list.html'):
    ''' List student records '''
    if request.method == 'POST':
        student_resource = PatientResource()
        dataset = Dataset()
        new_students = request.FILES['students']

        imported_data = dataset.load(new_students.read())
        result = student_resource.import_data(dataset, dry_run=True) # Test the data import

        if not result.has_errors():
            student_resource.import_data(dataset, dry_run=False) # Actually import now
   
    students = Patient.objects.filter(patient_type='s')
    data = {}
    data['students'] = students
    data['section'] = 'Students'

    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def student_add(request, template_name='management/records/students/add.html'):
    ''' Add student record '''
    form = PatientForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        student = form.save(commit=False)
        # do something
        student.patient_type = "s"
        student.save()
        messages.success(request, "{} {}'s record registered.".format(student.first_name, student.last_name))
        return redirect('student_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
    
    data = {}
    data['form'] = form
    data['section'] = 'New student'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def student_edit(request, pk, template_name='management/records/students/edit.html'):
    ''' Edit student record '''
    student = get_object_or_404(Patient, pk=pk)
    form = PatientForm(request.POST or None, request.FILES or None, instance=student)
    if form.is_valid():
        student = form.save(commit=False)
        # do something
        student.save()
        messages.success(request, "Changes to {} {}'s record saved.".format(student.first_name, student.last_name))
        return redirect('student_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['student'] = student
    data['section'] = 'Edit student'
    return render(request, template_name, data)

@login_required
def student_detail(request, pk, template_name='management/records/students/detail.html'):
    ''' View student record '''
    import_med(request)
    student = get_object_or_404(Patient, pk=pk)
    try:
	    latest_med_rec = MedicalRecord.objects.filter(patient=student).latest('checkup_date')
    except:
        latest_med_rec = []
    medical_records = MedicalRecord.objects.filter(patient=student)
    data = {}
    data['student'] = student
    data['latest_med_rec'] = latest_med_rec
    data['medical_records'] = medical_records
    data['section'] = 'Student Detail'
    return render(request, template_name, data)
# --

# Staff Member views
@login_required
def staff_member_list(request, template_name='management/records/staff_members/list.html'):
    ''' List Staff Member records '''
    if request.method == 'POST':
        staff_member_resource = PatientResource()
        dataset = Dataset()
        new_staff_members = request.FILES['staff_members']

        imported_data = dataset.load(new_staff_members.read())
        result = staff_member_resource.import_data(dataset, dry_run=True) # Test the data import

        if not result.has_errors():
            staff_member_resource.import_data(dataset, dry_run=False) # Actually import now
   
    staff_members = Patient.objects.filter(patient_type='sm')
    data = {}
    data['staff_members'] = staff_members
    data['section'] = 'Staff Members'

    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def staff_member_add(request, template_name='management/records/staff_members/add.html'):
    ''' Add Staff Member record '''
    form = PatientForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        staff_member = form.save(commit=False)
        # do something
        staff_member.patient_type = "sm"
        staff_member.save()
        messages.success(request, "{} {}'s record registered.".format(staff_member.first_name, staff_member.last_name))
        return redirect('staff_member_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
    
    data = {}
    data['form'] = form
    data['section'] = 'New Staff Member'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def staff_member_edit(request, pk, template_name='management/records/staff_members/edit.html'):
    ''' Edit Staff Member record '''
    staff_member = get_object_or_404(Patient, pk=pk)
    form = PatientForm(request.POST or None, request.FILES or None, instance=staff_member)
    if form.is_valid():
        staff_member = form.save(commit=False)
        # do something
        staff_member.save()
        messages.success(request, "Changes to {} {}'s record saved.".format(staff_member.first_name, staff_member.last_name))
        return redirect('staff_member_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['staff_member'] = staff_member
    data['section'] = 'Edit Staff Member'
    return render(request, template_name, data)

@login_required
def staff_member_detail(request, pk, template_name='management/records/staff_members/detail.html'):
    ''' View Staff Member record '''
    import_med(request)
    staff_member = get_object_or_404(Patient, pk=pk)
    try:
	    latest_med_rec = MedicalRecord.objects.filter(patient=staff_member).latest('checkup_date')
    except:
        latest_med_rec = []
    medical_records = MedicalRecord.objects.filter(patient=staff_member)
    data = {}
    data['staff_member'] = staff_member
    data['latest_med_rec'] = latest_med_rec
    data['medical_records'] = medical_records
    data['section'] = 'Staff Member Detail'
    return render(request, template_name, data)
# --

# Account views
@login_required
@user_passes_test(lambda u: u.is_staff)
def account_list(request, template_name='management/accounts/list.html'):
    ''' List user accounts '''
    accounts = User.objects.all()
    data = {}
    data['accounts'] = accounts
    data['section'] = 'Accounts'

    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def account_add(request, template_name='management/accounts/add.html'):
    ''' Add user account '''
    form = NewAccountForm(request.POST or None)
    if form.is_valid():
        account = form.save()
        messages.success(request, "{} {}'s account registered".format(account.first_name, account.last_name))
        return redirect('account_list')
    data = {}
    data['form'] = form
    data['section'] = 'New Account'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def account_edit(request, pk, template_name='management/accounts/edit.html'):
    ''' Edit user account '''
    account = get_object_or_404(User, pk=pk)
    form = AccountForm(request.POST or None, instance=account)
    if form.is_valid():
        account = form.save()
        messages.success(request, "Changes to {} {}'s account has been saved.".format(account.first_name, account.last_name))
        return redirect('account_list')
    data = {}
    data['form'] = form
    data['account'] = account
    data['section'] = 'Edit Account'
    return render(request, template_name, data)

@login_required
def account_profile(request, template_name='management/accounts/profile.html'):
    ''' View user account '''
    account = request.user
    form = AccountForm(request.POST or None, instance=account)
    if form.is_valid():
        account = form.save(commit=False)
        if account.is_staff:
            account.is_active = True
        account.save()
        messages.success(request, "Changes to your account has been saved.")
        return redirect('home')
    data = {}
    data['form'] = form
    data['account'] = account
    data['section'] = 'Profile'
    return render(request, template_name, data)
# --

# Medical Record Views
@login_required
def medical_record_list(request, template_name='management/records/medical/list.html'):
    ''' List medical records '''
    import_med(request)
    medical_records = MedicalRecord.objects.all()
    data = {}
    data['medical_records'] = medical_records
    data['section'] = 'Medical Records'
    
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def medical_record_add(request, template_name='management/records/medical/add.html'):
    ''' Add medical record '''
    form = MedicalRecordForm(request.POST or None)
    if form.is_valid():
        medical_record = form.save(commit=False)
        # do something
        medical_record.save()
        messages.success(request, "Medical Record {} for {} created.".format(medical_record.pk, medical_record.patient))
        return redirect('medical_record_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['section'] = 'New Medical Record'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def medical_for_student(request, pk, template_name='management/records/medical/student.html'):
    ''' Medical record for student '''
    student = get_object_or_404(Patient, pk=pk)
    medical_records = MedicalRecord.objects.filter(patient=student)
    form = PatientMedicalRecordForm(request.POST or None)
    if form.is_valid():
        medical_record = form.save(commit=False)
        medical_record.patient = student
        # do something
        medical_record.save()
        messages.success(request, 'Medical Record for {} created'.format(student.get_full_name()))
        return redirect('medical_for_student', pk=pk)
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['student'] = student
    data['medical_records'] = medical_records
    data['section'] = 'New Medical Record'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def medical_for_staff_member(request, pk, template_name='management/records/medical/staff_member.html'):
    ''' Medical record for staff_member '''
    staff_member = get_object_or_404(Patient, pk=pk)
    medical_records = MedicalRecord.objects.filter(patient=staff_member)
    form = PatientMedicalRecordForm(request.POST or None)
    if form.is_valid():
        medical_record = form.save(commit=False)
        medical_record.patient = staff_member
        # do something
        medical_record.save()
        messages.success(request, 'Medical Record for {} created'.format(staff_member.get_full_name()))
        return redirect('medical_for_staff_member', pk=pk)
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['staff_member'] = staff_member
    data['medical_records'] = medical_records
    data['section'] = 'New Medical Record'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def medical_record_edit(request, pk, template_name='management/records/medical/edit.html'):
    ''' Edit medical records '''
    medical_record = get_object_or_404(MedicalRecord, pk=pk)
    form = EditMedicalRecordForm(request.POST or None, instance=medical_record)
    if form.is_valid():
        medical_record = form.save(commit=False)
        medical_record.patient = medical_record.patient
        # do something
        medical_record.save()
        messages.success(request, "Changes to Medical Record {} for {} saved.".format(medical_record.pk, medical_record.patient))
        return redirect('medical_record_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['medical_record'] = medical_record
    data['section'] = 'Edit Medical Record'
    return render(request, template_name, data)
# --

# Condition Record Views
@login_required
@user_passes_test(lambda u: u.is_staff)
def condition_list(request, template_name='management/records/conditions/list.html'):
    
    if request.method == 'POST':
        condition_resource = ConditionResource()
        dataset = Dataset()
        new_conditions = request.FILES['conditions']

        imported_data = dataset.load(new_conditions.read())
        result = condition_resource.import_data(dataset, dry_run=True) # Test the data import

        if not result.has_errors():
            condition_resource.import_data(dataset, dry_run=False) # Actually import now
    
    conditions = Condition.objects.all()
    data = {}
    data['conditions'] = conditions
    data['section'] = 'Conditions'

    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def condition_add(request, template_name='management/records/conditions/add.html'):

    form = ConditionForm(request.POST or None)
    if form.is_valid():
        condition = form.save(commit=False)
        # do something
        condition.save()
        messages.success(request, "Condition {} record added.".format(condition.name))
        return redirect('condition_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['section'] = 'New condition'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def condition_edit(request, pk, template_name='management/records/conditions/edit.html'):

    condition = get_object_or_404(condition, pk=pk)
    form = ConditionForm(request.POST or None, instance=condition)
    if form.is_valid():
        condition = form.save(commit=False)
        # do something
        condition.save()
        messages.success(request, "condition {} registered.".format(condition.name))
        return redirect('condition_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['section'] = 'New condition'
    return render(request, template_name, data)
# --

# Grade Level Views
@login_required
@user_passes_test(lambda u: u.is_staff)
def grade_level_list(request, template_name='management/records/grade_levels/list.html'):
    
    if request.method == 'POST':
        grade_level_resource = GradeLevelResource()
        dataset = Dataset()
        new_grade_levels = request.FILES['grade_levels']

        imported_data = dataset.load(new_grade_levels.read())
        result = grade_level_resource.import_data(dataset, dry_run=True) # Test the data import

        if not result.has_errors():
            grade_level_resource.import_data(dataset, dry_run=False) # Actually import now
    
    grade_levels = GradeLevel.objects.all()
    data = {}
    data['grade_levels'] = grade_levels
    data['section'] = 'Grade Levels'

    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def grade_level_add(request, template_name='management/records/grade_levels/add.html'):

    form = GradeLevelForm(request.POST or None)
    if form.is_valid():
        grade_level = form.save(commit=False)
        # do something
        grade_level.save()
        messages.success(request, "Grade Level {} record added.".format(grade_level.grade_level))
        return redirect('grade_level_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['section'] = 'New grade level'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def grade_level_edit(request, pk, template_name='management/records/grade_levels/edit.html'):

    grade_level = get_object_or_404(GradeLevel, pk=pk)
    form = GradeLevelForm(request.POST or None, instance=grade_level)
    if form.is_valid():
        grade_level = form.save(commit=False)
        # do something
        grade_level.save()
        messages.success(request, "Grade Level {} modified.".format(grade_level.grade_level))
        return redirect('grade_level_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['section'] = 'Edit Grade Level'
    return render(request, template_name, data)
# --

# Section Views
@login_required
@user_passes_test(lambda u: u.is_staff)
def section_list(request, template_name='management/records/sections/list.html'):
    
    if request.method == 'POST':
        section_resource = SectionResource()
        dataset = Dataset()
        new_sections = request.FILES['sections']

        imported_data = dataset.load(new_sections.read())
        result = section_resource.import_data(dataset, dry_run=True) # Test the data import

        if not result.has_errors():
            section_resource.import_data(dataset, dry_run=False) # Actually import now
    
    sections = Section.objects.all()
    data = {}
    data['sections'] = sections
    data['section'] = 'Sections'

    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def section_add(request, template_name='management/records/sections/add.html'):

    form = SectionForm(request.POST or None)
    if form.is_valid():
        section = form.save(commit=False)
        # do something
        section.save()
        messages.success(request, "Section {} record added.".format(section.name))
        return redirect('section_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['section'] = 'New section'
    return render(request, template_name, data)

@login_required
@user_passes_test(lambda u: u.is_staff)
def section_edit(request, pk, template_name='management/records/sections/edit.html'):

    section = get_object_or_404(Section, pk=pk)
    form = SectionForm(request.POST or None, instance=section)
    if form.is_valid():
        section = form.save(commit=False)
        # do something
        section.save()
        messages.success(request, "Section {} modified.".format(section.name))
        return redirect('section_list')
    else:
        messages.info(request, "Please make sure to fill out all the required fields below")
		
    data = {}
    data['form'] = form
    data['section'] = 'Edit section'
    return render(request, template_name, data)
# --